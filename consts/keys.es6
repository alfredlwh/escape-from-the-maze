const KEYS = {
  STAGE_SELECTION: {
    'a': 'simple',
    'b': 'easy',
    'c': 'normal',
    'd': 'hard',
    'e': 'lunatic',
    'f': 'mines'
  }
};


export default KEYS;
