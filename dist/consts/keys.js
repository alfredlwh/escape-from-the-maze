'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true
});
var KEYS = {
  STAGE_SELECTION: {
    'a': 'simple',
    'b': 'easy',
    'c': 'normal',
    'd': 'hard',
    'e': 'lunatic',
    'f': 'mines'
  }
};

exports['default'] = KEYS;
module.exports = exports['default'];