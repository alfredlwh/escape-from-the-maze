import Model from 'models/Model';


export default class GameResultModel extends Model {

  constructor({ timeLimit, lastGameTime, minesCount }) {
    super();

    this._timeLimit = timeLimit;
    this._lastGameTime = lastGameTime;
    this._minesCount = minesCount;
  }

  calculateScore() {
    return (this._timeLimit - this._lastGameTime) * (this._minesCount == 0 ? 1 : (1 + this._minesCount / 10));
  }
}
