import ThingModel from 'models/things/ThingModel';


export default class MinesThingModel extends ThingModel {

  constructor() {
    super();

    this._symbol = '$';
    this._isPickable = true;
  }

  toContent() {
    return '{cyan-fg}' + this._symbol + '{/}';
  }
}

Object.assign(MinesThingModel, {
  typeId: 'mines'
});
